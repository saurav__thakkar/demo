import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:flutterinterviewdemo/controller/home_controller.dart';
import 'package:flutterinterviewdemo/custom_widget/HeroAnimation.dart';
import 'package:flutterinterviewdemo/custom_widget/TransparentRoute.dart';
import 'package:flutterinterviewdemo/custom_widget/full_screen_image.dart';
import 'package:flutterinterviewdemo/ui/image_detail.dart';
import 'package:flutterinterviewdemo/utility/comman_utils.dart';
import 'package:flutterinterviewdemo/utility/constant/color_constant.dart';
import 'package:flutterinterviewdemo/utility/constant/string_constant.dart';
import 'package:get/get.dart';
import 'package:vector_math/vector_math_64.dart';

class HomeView extends StatelessWidget {

  HomeController _homeController = Get.put(HomeController());

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(StringConstant.APPBAR_TITLE),
      ),
      body: Container(
        margin: EdgeInsets.all(10),
        child: Column(
          children: [
            GetX<HomeController>(
              init: HomeController(),
              builder: (context){
                return Text(_homeController.x.value.isEmpty ? StringConstant.LONG_PRESS_DELETED : _homeController.x.value);
              },
            ),
            SizedBox( height: 10,),
            TextFormField(
              controller: _homeController.searchTextController,
              keyboardType: TextInputType.text,
              style: TextStyle(color: ColorConstant.BLACK),
              validator:(value)=>value.trim().isEmpty?StringConstant.ENTER_TEXT:null,
              decoration: InputDecoration(
                hintStyle: TextStyle(color: ColorConstant.GREY),
                hintText: StringConstant.SEARCH,
                fillColor: ColorConstant.LIGHT_GREY,
                filled: true,
                border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(10)
                ),
                enabledBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(10),
                  borderSide: BorderSide(color: ColorConstant.BLACK)
                ),
              ),
              onChanged: (value){
                _homeController.filterList(value);
              },
            ),
            SizedBox( height: 20,),
            Expanded(child: Obx(()=> ListView.builder(
                itemCount: _homeController.list.length,
                itemBuilder: (context,index){
                  return rowCountry(context,index);
                }
            )))
          ],
        ),
      ),
    );
  }

  //Country row widget
  Widget rowCountry(BuildContext context, int index){
    return GestureDetector(
      onLongPress: (){_homeController.deleteCountry(index);},
      child: Card(
        child: Container(
          padding: EdgeInsets.all(10),
          child: Row(
            children: [
              Container(width:50,height:50,child: smallImage(_homeController.list[index].flag)),
              SizedBox(width: 20,),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(_homeController.list[index].name,),
                  Text(_homeController.list[index].alpha3Code),
                ],
              )
            ],
          ),
        ),
      ),
    );
  }

  Widget smallImage(String flag) => FullScreenWidget(
    child: Center(
      child: Hero(
        tag: flag,
        child: ClipRRect(
          borderRadius: BorderRadius.circular(0),
          child: SvgPicture.network(
            flag,
            height: 50,
            width: 50,
            placeholderBuilder: (BuildContext context) => Container(
              padding: const EdgeInsets.all(10.0),
              child: const CircularProgressIndicator(
                  strokeWidth: 2.0
              ),
            ),),
        ),
      ),
    ),
    flag: flag,
  );

  Widget smallImage1(String flag){
    return Container(
        height:40,
        width: 40,
        child: SvgPicture.network(
          flag,
          placeholderBuilder: (BuildContext context) => Container(
            padding: const EdgeInsets.all(10.0),
            child: const CircularProgressIndicator(
                strokeWidth: 2.0
            ),
          ),
        )
    );
  }

}
