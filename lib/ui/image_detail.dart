import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:flutterinterviewdemo/custom_widget/circle_view.dart';
import 'package:flutterinterviewdemo/models/CountryModel.dart';

class ImageDetail extends StatelessWidget {
  Data list;
  ImageDetail(this.list);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Container(
            child: Hero(
            tag: "avatar_" + list.name,
            child: ClipOval(
              child: SvgPicture.network(
                list.flag,
                height: 250,
                width: 250,
                placeholderBuilder: (BuildContext context) => Container(
                  child: const CircularProgressIndicator(
                      strokeWidth: 2.0
                  ),
                ),),
                clipper: MyClip(),
            ),
          )
        ),
      ),
    );
  }
}
