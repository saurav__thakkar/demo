import 'package:flutter/material.dart';
import 'package:flutterinterviewdemo/ui/home.dart';
import 'package:get/get.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      title: "Interview Demo",
      debugShowCheckedModeBanner: false,
      home: HomeView(),
    );
  }
}

