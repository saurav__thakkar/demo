
import 'package:flutter/material.dart';

class CommonUtils{

  static void printWrapped(String text) {
    final pattern = new RegExp('.{1,800}'); // 800 is the size of each chunk
    pattern.allMatches(text).forEach((match) => print(match.group(0)));
  }

  static void showToast(BuildContext context,String msg){
    var scaffold = Scaffold.of(context);
    scaffold.showSnackBar(
        SnackBar(
          content:Text(msg),
          action: null,
        )
    );
  }
}