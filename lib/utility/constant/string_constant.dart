
abstract class StringConstant{
  static const APP_NAME = "Country";
  static const APPBAR_TITLE = "Countries";
  static const SEARCH = "Search here";
  static const ENTER_TEXT = "Enter text";
  static const NO_INTERNET = "No internet connection";
  static const COUNTRY_DELETED = "Country deleted succesfully.";
  static const LONG_PRESS_DELETED = "Long press to delete data.";
}