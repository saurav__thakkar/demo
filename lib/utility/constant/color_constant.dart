
import 'package:flutter/material.dart';

abstract class ColorConstant{

  static const BLACK = Colors.black;
  static const WHITE = Colors.white;
  static const GREY = Colors.grey;
  static const LIGHT_GREY = Color(0xFFEEEEEE);
}