
import 'package:flutter/material.dart';
import 'package:flutterinterviewdemo/data/local/manager/country_list_management.dart';
import 'package:flutterinterviewdemo/data/network/api_listener.dart';
import 'package:flutterinterviewdemo/data/network/web_services.dart';
import 'package:flutterinterviewdemo/models/CountryModel.dart';
import 'package:flutterinterviewdemo/utility/comman_utils.dart';
import 'package:flutterinterviewdemo/utility/constant/color_constant.dart';
import 'package:flutterinterviewdemo/utility/constant/string_constant.dart';
import 'package:get/get.dart';
import 'package:path/path.dart';

class HomeController extends GetxController implements ApiListener{

  TextEditingController searchTextController;
  var list = List<Data>().obs;
  var originalList = List<Data>();
  /*RxString*/var  x = "".obs;

  @override
  void onInit() {
    searchTextController = TextEditingController();
    super.onInit();
  }

  @override
  void onClose() {
    searchTextController.dispose();
  }

  @override
  void onReady() async {
    super.onReady();
    List<Data> list = await CountryListManager.instance.getCountryList();
    if(list.isEmpty){
      Get.dialog(Center(child: CircularProgressIndicator()),barrierDismissible: false);
      WebServices(this).getCountryList();
    }else{
      originalList.addAll(list);
      this.list.addAll(list);
    }
  }

  @override
  void onApiFailure(String statusCode, mObject) {
  }

  @override
  void onApiSuccess(String statusCode, mObject) {
    if(mObject is CountryModel){
      list.addAll(mObject.data);
      originalList.addAll(mObject.data);

      for(var i = 0;i<originalList.length;i++){
        CountryListManager.instance.insertData(originalList[i]);
      }
    }
    Get.back();
  }

  @override
  void onException() {
  }

  @override
  void onNotInternetConnection() {
  }

  @override
  void setLoadingState(bool isShow) {
  }

  void filterList(String text){
    list.clear();
    if(text.isEmpty){
      list.addAll(originalList);
    }else{
      for(var i = 0;i<originalList.length;i++){
        if(originalList[i].name.toLowerCase().contains(text.toLowerCase())){
          list.add(originalList[i]);
        }
      }
    }
  }

  void deleteCountry(int index){
    CountryListManager.instance.deleteCountry(originalList[index]);
    originalList.removeAt(index);
    this.list.removeAt(index);
    print('Delete ::::::::::::::::');
    x.value = StringConstant.COUNTRY_DELETED;
    Get.snackbar(StringConstant.APP_NAME, StringConstant.COUNTRY_DELETED,snackPosition: SnackPosition.BOTTOM,colorText: ColorConstant.WHITE,backgroundColor: ColorConstant.BLACK);
    new Future.delayed(Duration(seconds: 5),(){
      x.value = StringConstant.LONG_PRESS_DELETED;
    });
  }
}