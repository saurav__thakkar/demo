import 'dart:async';
import 'dart:io';
import 'package:flutterinterviewdemo/data/local/database_constant.dart';
import 'package:path/path.dart';
import 'package:path_provider/path_provider.dart';
import 'package:sqflite/sqflite.dart';

class DatabaseHelper{

  DatabaseHelper._internal();
  static DatabaseHelper instance = DatabaseHelper._internal();
  factory DatabaseHelper() => instance;

  static Database _database;

  Future<Database> get db async{
    if(_database != null){
      return _database;
    }else{
      _database = await init();
      return _database;
    }
  }

  Future<Database> init() async{
    Directory directory = await getApplicationDocumentsDirectory();
    var dbPath = join(directory.path,DatabaseConstant.DATABASE_NAME);
    var database = openDatabase(
      dbPath,
      version: DatabaseConstant.DATABASE_VERSION,
      onCreate: _onCreate,
      onUpgrade: _onUpgrade,
    );
    return database;
  }

  void _onCreate(Database db,int version){
    db.execute(DatabaseQuery.CREATE_TABLE_COUNTRY);
    print('Create table');
  }

  void _onUpgrade(Database db,int oldVersion,int newVersion){

  }

  void closeDB() async{
    var client = await db;
    client.close();
  }

  //Remove all table
  void removeAllData() async{
    var client = await db;
    client.delete(DatabaseTable.TABLE_COUNTRY);
  }
}