abstract class DatabaseConstant{
  static const DATABASE_NAME = "test.db";
  static const DATABASE_VERSION = 1;
}

abstract class DatabaseQuery{

  //Query create country table
  static const CREATE_TABLE_COUNTRY ='''
  CREATE TABLE ${DatabaseTable.TABLE_COUNTRY}(
  ${DatabaseColumn.COLUMN_COUNTRY_ID} INTEGER PRIMARY KEY AUTOINCREMENT,
  ${DatabaseColumn.COLUMN_COUNTRY_NAME} TEXT,
  ${DatabaseColumn.COLUMN_COUNTRY_CODE} TEXT,
  ${DatabaseColumn.COLUMN_COUNTRY_FLAG} TEXT)
  ''';
}

abstract class DatabaseTable{
  static const TABLE_COUNTRY = "countrys";
}

abstract class DatabaseColumn{

  //country column
  static const COLUMN_COUNTRY_ID = "country_id";
  static const COLUMN_COUNTRY_NAME = "name";
  static const COLUMN_COUNTRY_CODE = "alpha3Code";
  static const COLUMN_COUNTRY_FLAG = "flag";
}