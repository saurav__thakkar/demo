
import 'package:flutterinterviewdemo/data/local/database_constant.dart';
import 'package:flutterinterviewdemo/data/local/database_helper.dart';
import 'package:flutterinterviewdemo/models/CountryModel.dart';
import 'package:sqflite/sqflite.dart';

class CountryListManager{
  CountryListManager._internal();
  static CountryListManager instance = CountryListManager._internal();
  factory CountryListManager() => instance;

  ///Insert data
  Future<int> insertData(Data data) async{
    var client = await DatabaseHelper.instance.db;
    return client.insert(DatabaseTable.TABLE_COUNTRY, data.toJson(),
    conflictAlgorithm: ConflictAlgorithm.replace);
  }

  ///Update data
  Future<int> updateData(Data data) async{
    var client = await DatabaseHelper.instance.db;
    return client.update(DatabaseTable.TABLE_COUNTRY, data.toJson(),
    where: '${DatabaseColumn.COLUMN_COUNTRY_NAME} = ?',
    whereArgs: [data.name]);
  }

  ///Delete data
  Future<int> deleteCountry(Data data) async{
    var client = await DatabaseHelper.instance.db;
    return client.delete(DatabaseTable.TABLE_COUNTRY,
    where: '${DatabaseColumn.COLUMN_COUNTRY_NAME} = ?',
    whereArgs: [data.name]);
  }

  ///Get All list
  Future<List<Data>> getCountryList() async{
    var client = await DatabaseHelper.instance.db;
    List<Map<String,dynamic>> list = await client.rawQuery('SELECT * FROM ${DatabaseTable.TABLE_COUNTRY}');

    if(list.isNotEmpty){
      var countryList = list.map((e) => Data.fromJson(e)).toList();
      return countryList;
    }
    return [];
  }

  ///Get List Reverse order
  Future<List<Data>> getCountryListInReverseOrder() async{
    var client = await DatabaseHelper.instance.db;
    List<Map<String,dynamic>> list = await client.rawQuery('SELECT * FROM ${DatabaseTable.TABLE_COUNTRY} ORDER BY ${DatabaseColumn.COLUMN_COUNTRY_NAME} DESC');

    if(list.isNotEmpty){
      var countryList = list.map((e) => Data.fromJson(e)).toList();
      return countryList;
    }
    return [];
  }

  ///Delete table
  Future<int> deleteTable() async{
    var client = await DatabaseHelper.instance.db;
    return client.delete(DatabaseTable.TABLE_COUNTRY);
  }
}