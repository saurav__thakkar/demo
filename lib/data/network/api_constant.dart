abstract class ApiEnvironment{
  static const PRODUCTION = "https://restcountries.eu/rest/v2/";
}

abstract class ApiResponse{
  static const STATUS = "status";
  static const STATUS_CODE = "status_code";
  static const DATA = "data";
  static const SUCCESS = "success";
  static const FAILURE = "failure";
  static const EXCEPTION = "exception";
  static const NO_INTERNET = "no_internet";
}

abstract class Apis{
  static const COUNTRY_LIST = "all";
}

abstract class ApiParam{

}