
import 'dart:collection';

import 'package:flutterinterviewdemo/data/network/api_constant.dart';
import 'package:flutterinterviewdemo/data/network/network_check.dart';
import 'package:flutterinterviewdemo/utility/comman_utils.dart';
import 'package:flutterinterviewdemo/utility/constant/string_constant.dart';
import 'package:http/http.dart' as http;

enum HttpMethod{ GET , POST }
class NetworkProvider{
  NetworkProvider._internal();
  static NetworkProvider instant = NetworkProvider._internal();

  factory NetworkProvider() => instant;

  NetworkCheck networkCheck = NetworkCheck();

  Future<dynamic> request({String url,
  HttpMethod method,
  Map<dynamic,dynamic> body,
  Map<dynamic,dynamic> header}) async{

    final Map<dynamic,dynamic> mapNetworkApiStatus = HashMap();
    final isConnect = await networkCheck.check();

    if(method != HttpMethod.GET){
      CommonUtils.printWrapped("Body : $body");
    }

    if(isConnect){
      try {
        final request = method == HttpMethod.GET ? await http.get(ApiEnvironment.PRODUCTION + url)
            : await http.post(ApiEnvironment.PRODUCTION + url,body: body);
        print("URL : ${request.request.url}");
        print("Status code : ${request.statusCode}");
        CommonUtils.printWrapped("Response body : ${request.body.toString()}");

        if(request.statusCode >= 200 && request.statusCode < 400){
          mapNetworkApiStatus[ApiResponse.STATUS] = ApiResponse.SUCCESS;
          mapNetworkApiStatus[ApiResponse.DATA] = request.body.toString();
          mapNetworkApiStatus[ApiResponse.STATUS_CODE] = request.statusCode.toString();
        }else{
          mapNetworkApiStatus[ApiResponse.DATA] = request.body.toString();
          mapNetworkApiStatus[ApiResponse.STATUS] = ApiResponse.FAILURE;
          mapNetworkApiStatus[ApiResponse.STATUS_CODE] = request.statusCode.toString();
        }
        return mapNetworkApiStatus;
      } catch (error) {
        mapNetworkApiStatus[ApiResponse.STATUS] = ApiResponse.EXCEPTION;
        mapNetworkApiStatus[ApiResponse.DATA]= error.toString();
        mapNetworkApiStatus[ApiResponse.STATUS_CODE] = "";
        return mapNetworkApiStatus;
      }
    }else{
      mapNetworkApiStatus[ApiResponse.STATUS] = ApiResponse.NO_INTERNET;
      mapNetworkApiStatus[ApiResponse.DATA] = StringConstant.NO_INTERNET;
      return mapNetworkApiStatus;
    }
  }

}