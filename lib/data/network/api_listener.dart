abstract class ApiListener{
  void setLoadingState(bool isShow);
  void onApiSuccess(String statusCode, dynamic mObject);
  void onApiFailure(String statusCode, dynamic mObject);
  void onException();
  void onNotInternetConnection();
}