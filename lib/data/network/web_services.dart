

import 'dart:convert';

import 'package:flutterinterviewdemo/data/network/api_constant.dart';
import 'package:flutterinterviewdemo/data/network/api_listener.dart';
import 'package:flutterinterviewdemo/data/network/network_provider.dart';
import 'package:flutterinterviewdemo/models/CountryModel.dart';

class WebServices{
  ApiListener apiListener;

  WebServices(this.apiListener);

  void _onApiResponse(String statusCode,String status,dynamic data){
    if(status == ApiResponse.SUCCESS){
      apiListener.onApiSuccess(statusCode, data);
    }
    if(status == ApiResponse.FAILURE){
      apiListener.onApiFailure(statusCode, data);
    }
    if(status == ApiResponse.EXCEPTION){
      apiListener.onException();
    }
    if(status == ApiResponse.NO_INTERNET){
      apiListener.onNotInternetConnection();
    }
  }

  void getCountryList() async{

    final request = await NetworkProvider.instant.request(url: Apis.COUNTRY_LIST,method: HttpMethod.GET);

    Object apiData = "{\"data\":"+request[ApiResponse.DATA]+"}";
    print("api data : $apiData");
    if(request[ApiResponse.STATUS] == ApiResponse.SUCCESS){
      apiData = CountryModel.fromJson(json.decode(apiData));
    }

    _onApiResponse(request[ApiResponse.STATUS_CODE],request[ApiResponse.STATUS],apiData);
  }
}