import 'package:flutter/material.dart';

class MyClip extends CustomClipper<Rect> {
  Rect getClip(Size size) {
    return Rect.fromLTWH(55, 0, 250, 250);
  }

  bool shouldReclip(oldClipper) {
    return false;
  }
}